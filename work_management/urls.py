from django.conf.urls import url
from django.urls import path
from work_management import views as customer_views

urlpatterns = [
    path('signup/', customer_views.SignUp.as_view(), name='signup'),
    path(
        'change_password/',
        customer_views.change_password,
        name='change_password'
    ),
    path('', customer_views.index, name='index')
]
