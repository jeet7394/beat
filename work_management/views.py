from django.contrib.auth import authenticate, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.views import login
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from work_management.forms import SignUpForm


class SignUp(TemplateView):
    """
    Get signup form and post signup data

    """
    @staticmethod
    def get(request):
        """ Get signup form """
        if request.user.is_authenticated:
            return redirect('/')
        form = SignUpForm(request.GET)
        return render(request, 'registration/signup.html', {'form': form})

    @staticmethod
    def post(request):
        """
        Check validity of form and return HTTPResponsce with help of render.
        """
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            email = form.cleaned_data.get('email')
            user = authenticate(
                username=username,
                password=raw_password,
                email=email
            )
            login(request, user)
            return redirect('/')
        return render(request, 'registration/signup.html', {'form': form})


@login_required(login_url='/')
def change_password(request):
    """
        Change user password and check validity of form and return,
        HTTPResponsce with help of render.
    """
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(
                request,
                'Your password was successfully updated!'
            )
            return redirect('/')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/change_password.html', {
        'form': form
    })


def user_login(request):
    """
    Check user are login or not.
    If already login redirect at home.
    If not then open login page.
    """
    if request.user.is_authenticated:
        return redirect('/')
    return login(request)


@login_required
def index(request):
    return render(request, 'index.html')
