from dal import autocomplete
from master.models import State, District, ProductGroup, Unit, Designation, \
    Town, Employee, DatesManager, Product, CompanyKeyPerson, \
    Company, Distributor
from django.utils.safestring import mark_safe
from django.urls import reverse
from django.forms import widgets
from beat import settings


class RelatedFieldWidgetCanAdd(autocomplete.ModelSelect2):

    def __init__(self, related_model, related_url=None, *args, **kw):

        super(RelatedFieldWidgetCanAdd, self).__init__(*args, **kw)
        self.custom_url = False
        if not related_url:
            related_url = '/master/{0}/?add'.format(
                related_model.__name__.lower()
            )
            self.custom_url = True

        # Be careful that here "reverse" is not allowed
        self.related_url = related_url

    def render(self, name, value, attrs=None):
        """Calling Django render together with `render_forward_conf`."""
        widget = super(RelatedFieldWidgetCanAdd, self).render(
            name, value, attrs)
        conf = """
        <div class="uk-inline uk-width-1-1">
            {1}
            <a href="#" class="uk-form-icon uk-form-icon-flip" uk-icon="icon: plus" onClick="openModel('{0}');"></a>
        </div>
        """.format(self.related_url, widget)
        return mark_safe(conf)


class MultipleRelatedFieldWidgetCanAdd(autocomplete.ModelSelect2Multiple):
    def __init__(self, related_model, related_url=None, *args, **kw):

        super(MultipleRelatedFieldWidgetCanAdd, self).__init__(*args, **kw)
        self.custom_url = False
        if not related_url:
            related_url = '/master/{0}/?add'.format(
                related_model.__name__.lower()
            )
            self.custom_url = True

        # Be careful that here "reverse" is not allowed
        self.related_url = related_url

    def render(self, name, value, attrs=None):
        """Calling Django render together with `render_forward_conf`."""
        widget = super(MultipleRelatedFieldWidgetCanAdd, self).render(
            name, value, attrs)
        conf = """
        <div class="uk-inline uk-width-1-1">
            {1}
            <a href="#" class="uk-form-icon uk-form-icon-flip" uk-icon="icon: plus" onClick="openModel('{0}');"></a>
        </div>
        """.format(self.related_url, widget)
        return mark_safe(conf)


class StateAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = State.objects.all().order_by('name')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class DistrictAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = District.objects.all().order_by('name')
        state = self.forwarded.get(
            'state',
            self.forwarded.get(
                'headquarter_state',
                None
            )
        )
        if state:
            qs = qs.filter(state_id=int(state))

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class TownAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Town.objects.all().order_by('name')

        district = self.forwarded.get(
            'district',
            self.forwarded.get(
                'headquarter_district',
                None
            )
        )
        if district:
            qs = qs.filter(district_id=int(district))
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class ProductAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Product.objects.all().order_by('name')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class ProductGroupAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = ProductGroup.objects.all().order_by('name')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class UnitAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Unit.objects.all().order_by('name')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class DesignationAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Designation.objects.all().order_by('name')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class EmployeeDetailAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Employee.objects.all().order_by('name')
        designation = self.forwarded.get('designation', None)

        if designation:
            qs = qs.filter(designation=Designation.objects.get(
                id=int(designation)).reporting_designation
            )

        if self.forwarded.get('field_staff', None):
            qs = qs.filter(designation__work_area='field')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class CompanyKeyPersonAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = CompanyKeyPerson.objects.all().order_by('name')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class CompanyAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Company.objects.all().order_by('firm_name')
        import pdb; pdb.set_trace()
        if self.q:
            qs = qs.filter(firm_name__istartswith=self.q)
        return qs


class DistributorAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Distributor.objects.all().order_by('firm_name')

        if self.forwarded.get('hub', None):
            qs = qs.filter(onward_supply=True)
        if self.forwarded.get('designation', None):
            qs = qs.filter(appointing_cease_date__end_date__isnull=True)

        if self.forwarded.get('show_only_field_staff', None):
            qs = qs.filter(field_staff=None)

        if self.q:
            qs = qs.filter(firm_name__istartswith=self.q)
        return qs
