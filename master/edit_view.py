from django.shortcuts import HttpResponse, Http404
from django.db.models import ProtectedError
from django.shortcuts import render, redirect
from django.views import View
from django.forms import formset_factory
from master import forms
from master import models
from master.utils import generate_date_dict, generate_tax_dict, \
    generate_key_person_dict


def delete(request, model, pk):
    model_table = getattr(models, model)
    obj = model_table.objects.get(id=pk)
    try:
        obj.delete()
        return HttpResponse(obj)
    except ProtectedError:
        return HttpResponse(model, status=409)


class EditState(View):

    def get_object(self, pk):
        try:
            return models.State.objects.get(id=pk)
        except:
            raise Http404

    def get(self, request, pk):
        obj = self.get_object(pk)
        form = forms.StateForm(instance=obj)
        return render(request, 'state/edit_state.html', {'form': form})

    def post(self, request, pk):
        obj = self.get_object(pk)
        form = forms.StateForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            return redirect('/master/state/')
        return render(request, 'state/edit_state.html', {'form': form})


class EditDistrict(View):

    def get_object(self, pk):
        try:
            return models.District.objects.get(id=pk)
        except:
            raise Http404

    def get(self, request, pk):
        obj = self.get_object(pk)
        form = forms.DistrictForm(instance=obj)
        return render(request, 'district/edit_district.html', {'form': form})

    def post(self, request, pk):
        obj = self.get_object(pk)
        form = forms.DistrictForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            return redirect('/master/district/')
        return render(request, 'district/edit_district.html', {'form': form})


class EditTown(View):

    def get_object(self, pk):
        try:
            return models.Town.objects.get(id=pk)
        except:
            raise Http404

    def get(self, request, pk):
        obj = self.get_object(pk)
        form = forms.TownForm(instance=obj)
        return render(request, 'town/edit_town.html', {'form': form})

    def post(self, request, pk):
        obj = self.get_object(pk)
        form = forms.TownForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            return redirect('/master/town/')
        return render(request, 'town/edit_town.html', {'form': form})


class EditUnit(View):

    def get_object(self, pk):
        try:
            return models.Unit.objects.get(id=pk)
        except:
            raise Http404

    def get(self, request, pk):
        obj = self.get_object(pk)
        form = forms.UnitForm(instance=obj)
        return render(request, 'unit/edit_unit.html', {'form': form})

    def post(self, request, pk):
        obj = self.get_object(pk)
        form = forms.UnitForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            return redirect('/master/unit/')
        return render(request, 'unit/edit_unit.html', {'form': form})


class EditProductGroup(View):

    def get_object(self, pk):
        try:
            return models.ProductGroup.objects.get(id=pk)
        except:
            raise Http404

    def get(self, request, pk):
        obj = self.get_object(pk)
        form = forms.ProductGroupForm(instance=obj)
        date_form = formset_factory(
            forms.DatesManagerForm,
            extra=1,
            formset=forms.BaseDatesFormSet
        )(generate_date_dict(obj, 'launch_discontinuation_date'), prefix='dates')
        tax_form = formset_factory(
            forms.TaxForm,
            extra=1,
            can_delete=True,
            validate_min=True
        )(generate_tax_dict(obj, 'tax'), prefix='tax')
        return render(request, 'product_group/edit_product_group.html', {
            'form': form,
            'date_form': date_form,
            'tax_form': tax_form
        })

    def post(self, request, pk):
        obj = self.get_object(pk)
        form = forms.ProductGroupForm(request.POST, instance=obj)
        date_form = formset_factory(
            forms.DatesManagerForm,
            extra=1,
            formset=forms.BaseDatesFormSet
        )(request.POST, prefix='dates')
        tax_form = formset_factory(
            forms.TaxForm,
            extra=1,
            formset=forms.BaseTaxFormSet
        )(request.POST, prefix='tax')
        form_error = False
        if form.is_valid() and tax_form.is_valid() and date_form.is_valid():
            for tax in tax_form:
                if not tax.is_valid():
                    form_error = True
            for date in date_form:
                if not date.is_valid():
                    form_error = True
            if not form_error:
                p_g = form.save()
                p_g.tax.clear()
                p_g.launch_discontinuation_date.clear()

                for tax in tax_form:
                    new_tax = tax.save()
                    p_g.tax.add(new_tax)

                for date in date_form:
                    new_date = date.save()
                    p_g.launch_discontinuation_date.add(new_date)
            return redirect('/master/productgroup/')
        return render(request, 'product_group/edit_product_group.html', {
            'form': form,
            'date_form': date_form,
            'tax_form': tax_form
        })


class EditProduct(View):

    def get_object(self, pk):
        try:
            return models.Product.objects.get(id=pk)
        except:
            raise Http404

    def get(self, request, pk):
        obj = self.get_object(pk)
        form = forms.ProductForm(instance=obj, product_id=pk)
        date_form = formset_factory(
            forms.DatesManagerForm,
            extra=1,
            formset=forms.BaseDatesFormSet
        )(generate_date_dict(obj, 'launch_discontinuation_date'), prefix='dates')
        return render(request, 'product/add_product.html', {
            'form': form,
            'date_form': date_form,
        })

    def post(self, request, pk):
        obj = self.get_object(pk)
        form = forms.ProductForm(request.POST, instance=obj, product_id=pk)
        date_form = formset_factory(
            forms.DatesManagerForm,
            extra=1,
            formset=forms.BaseDatesFormSet
        )(request.POST, prefix='dates')
        form_error = False
        if form.is_valid() and date_form.is_valid():
            for date in date_form:
                if not date.is_valid():
                    form_error = True
            if not form_error:
                p_g = form.save()
                p_g.launch_discontinuation_date.clear()

                for date in date_form:
                    new_date = date.save()
                    p_g.launch_discontinuation_date.add(new_date)
            return redirect('/master/product/')
        return render(request, 'product/add_product.html', {
            'form': form,
            'date_form': date_form,
        })


class EditDesignation(View):

    def get_object(self, pk):
        try:
            return models.Designation.objects.get(id=pk)
        except:
            raise Http404

    def get(self, request, pk):
        obj = self.get_object(pk)
        form = forms.DesignationForm(instance=obj)
        return render(request, 'designation/edit_designation.html', {
            'form': form
        })

    def post(self, request, pk):
        obj = self.get_object(pk)
        form = forms.DesignationForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            return redirect('/master/designation/')
        return render(request, 'designation/edit_designation.html', {
            'form': form
        })


class EditEmployee(View):

    def get_object(self, pk):
        try:
            return models.Employee.objects.get(id=pk)
        except:
            raise Http404

    def get(self, request, pk):
        obj = self.get_object(pk)
        form = forms.EmployeeForm(instance=obj)
        date_form = formset_factory(
            forms.DatesManagerForm,
            extra=1,
            formset=forms.BaseDatesFormSet
        )(generate_date_dict(obj, 'joining_leaving_date'), prefix='dates')
        return render(request, 'employee/edit_employee.html', {
            'form': form,
            'date_form': date_form
        })

    def post(self, request, pk):
        obj = self.get_object(pk)
        form = forms.EmployeeForm(request.POST, instance=obj)
        date_form = formset_factory(
            forms.DatesManagerForm,
            extra=1,
            formset=forms.BaseDatesFormSet
        )(request.POST, prefix='dates')
        form_error = False
        if form.is_valid() and date_form.is_valid():
            for date in date_form:
                if not date.is_valid():
                    form_error = True
            if not form_error:
                p_g = form.save()
                p_g.joining_leaving_date.clear()

                for date in date_form:
                    new_date = date.save()
                    p_g.joining_leaving_date.add(new_date)
            return redirect('/master/employee/')
        return render(request, 'employee/edit_employee.html', {
            'form': form,
            'date_form': date_form
        })


class EditCompany(View):

    def get_object(self, pk):
        try:
            return models.Company.objects.get(id=pk)
        except:
            raise Http404

    def get(self, request, pk):
        obj = self.get_object(pk)
        form = forms.CompanyForm(instance=obj)
        date_form = formset_factory(
            forms.DatesManagerForm,
            extra=1,
            formset=forms.BaseDatesFormSet
        )(generate_date_dict(obj, 'appointing_cease_date'), prefix='dates')
        key_person_form = formset_factory(
            forms.CompanyKeyPersonForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=forms.BaseDatesFormSet
        )(generate_key_person_dict(obj, 'key_person'), prefix='key_person')
        return render(request, 'company/edit_company.html', {
            'form': form,
            'date_form': date_form,
            'key_person_form': key_person_form
        })

    def post(self, request, pk):
        obj = self.get_object(pk)
        form = forms.CompanyForm(request.POST, instance=obj)
        date_form = formset_factory(
            forms.DatesManagerForm,
            extra=1,
            formset=forms.BaseDatesFormSet
        )(request.POST, prefix='dates')

        key_person_form = formset_factory(
            forms.CompanyKeyPersonForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=forms.BaseDatesFormSet
        )(request.POST, prefix='key_person')

        form_error = False
        if form.is_valid() and date_form.is_valid():
            for date in date_form:
                if not date.is_valid():
                    form_error = True

            for key in key_person_form:
                if not key.is_valid():
                    form_error = True

            if not form_error:
                company = form.save()
                company.appointing_cease_date.clear()

                for date in date_form:
                    new_date = date.save()
                    company.appointing_cease_date.add(new_date)

                for key in key_person_form:
                    new_key = key.save()
                    company.key_person.add(new_key)
            return redirect('/master/company/')

        return render(request, 'company/edit_company.html', {
            'form': form,
            'date_form': date_form,
            'key_person_form': key_person_form
        })


class EditDistributor(View):

    def get_object(self, pk):
        try:
            return models.Distributor.objects.get(id=pk)
        except:
            raise Http404

    def get(self, request, pk):
        obj = self.get_object(pk)
        form = forms.DistributorForm(instance=obj)
        date_form = formset_factory(
            forms.DatesManagerForm,
            extra=1,
            formset=forms.BaseDatesFormSet
        )(generate_date_dict(obj, 'appointing_cease_date'), prefix='dates')
        key_person_form = formset_factory(
            forms.CompanyKeyPersonForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=forms.BaseDatesFormSet
        )(generate_key_person_dict(obj, 'key_person'), prefix='key_person')
        return render(request, 'distributor/edit_distributor.html', {
            'form': form,
            'date_form': date_form,
            'key_person_form': key_person_form
        })

    def post(self, request, pk):
        obj = self.get_object(pk)
        form = forms.DistributorForm(request.POST, instance=obj)
        date_form = formset_factory(
            forms.DatesManagerForm,
            extra=1,
            formset=forms.BaseDatesFormSet
        )(request.POST, prefix='dates')

        key_person_form = formset_factory(
            forms.CompanyKeyPersonForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=forms.BaseDatesFormSet
        )(request.POST, prefix='key_person')

        form_error = False
        if form.is_valid() and date_form.is_valid():
            for date in date_form:
                if not date.is_valid():
                    form_error = True

            for key in key_person_form:
                if not key.is_valid():
                    form_error = True

            if not form_error:
                distributor = form.save()
                distributor.appointing_cease_date.clear()

                for date in date_form:
                    new_date = date.save()
                    distributor.appointing_cease_date.add(new_date)

                for key in key_person_form:
                    new_key = key.save()
                    distributor.key_person.add(new_key)
            return redirect('/master/distributor/')

        return render(request, 'distributor/edit_distributor.html', {
            'form': form,
            'date_form': date_form,
            'key_person_form': key_person_form
        })