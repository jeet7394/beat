from django import forms
from django.forms import widgets, ValidationError
from dal import autocomplete, forward
from djangoformsetjs.utils import formset_media_js
from django.forms import BaseFormSet
from master.models import State, Town, Company, DatesManager, District, Unit, \
    ProductGroup, Product, Designation, Employee, CompanyKeyPerson,\
    Distributor, TaxManager
from master.auto_complete import RelatedFieldWidgetCanAdd,\
    MultipleRelatedFieldWidgetCanAdd


class TownForm(forms.ModelForm):

    class Meta:
        model = Town
        fields = ('__all__')
        widgets = {
            'state':
            RelatedFieldWidgetCanAdd(
                related_model=State, url='/master/auto-complete/state/'),
            'district':
            RelatedFieldWidgetCanAdd(
                related_model=District, url='/master/auto-complete/district/',
                forward=['state', 'state_in']
            )
        }


class CompanyForm(forms.ModelForm):
    pin_code = forms.IntegerField(max_value=999999)
    std_code = forms.IntegerField(max_value=9999999)

    class Meta:
        model = Company
        fields = (
            'code', 'firm_name', 'address', 'state', 'district', 'town',
            'pin_code', 'std_code', 'phone_number_1', 'phone_number_2',
            'fssai_number', 'fssai_attachement', 'gst_number',
            'gst_attachement', 'pan_number', 'pan_attachment',
            'bank_name', 'account_number', 'isfc_code', 'branch',
            'product_group'
        )
        widgets = {
            'state':
            RelatedFieldWidgetCanAdd(
                related_model=State, url='/master/auto-complete/state/',
                related_url='/master/state/?add'
            ),
            'district':
            RelatedFieldWidgetCanAdd(
                related_model=District, url='/master/auto-complete/district/',
                forward=['state']
            ),
            'town':
            RelatedFieldWidgetCanAdd(
                related_model=Town, url='/master/auto-complete/town/',
                related_url='/master/town/?add',
                forward=['district']
            ),
            'product_group':
            MultipleRelatedFieldWidgetCanAdd(
                related_model=ProductGroup,
                url='/master/auto-complete/product-group/'
            )
        }


class BaseDatesFormSet(BaseFormSet):
    def clean(self):
        if any(self.errors):
            return

        if not self.forms:
            raise ValidationError(
                """Invalid dates
                    Add Atleast One row of Date"""
            )

        if None in [form.cleaned_data.get('start_date') for form in self.forms]:
            raise ValidationError(
                """Invalid dates
                    First date cant be blank"""
            )

        for i in range(0, len(self.forms)-1):
            end_date = self.forms[i].cleaned_data.get('end_date')
            start_date = self.forms[i+1].cleaned_data.get('start_date')
            if start_date and end_date:
                if start_date < end_date:
                    raise ValidationError(
                        """Invalid dates
                            End date of first row should be smaller
                            than start date of second"""
                    )


class BaseTaxFormSet(BaseFormSet):
    def clean(self):
        if any(self.errors):
            return

        for i in range(0, len(self.forms)-1):
            wef_date = self.forms[i].cleaned_data.get('wef_date')
            wef_date2 = self.forms[i+1].cleaned_data.get('wef_date')
            if wef_date and wef_date2:
                if wef_date2 < wef_date:
                    raise ValidationError(
                        """Invalid dates
                            End date of first row should be smaller
                            than start date of second"""
                    )


class DatesManagerForm(forms.ModelForm):

    class Meta:
        model = DatesManager
        fields = ('__all__')

    class Media(object):
        js = formset_media_js

    def clean(self):
        cleaned_data = super().clean()
        start_date = cleaned_data.get('start_date')
        end_date = cleaned_data.get('end_date')
        if start_date and end_date:
            if start_date >= end_date:
                self.add_error(
                    'start_date',
                    'Launch date Should be Smaller Than discontinuation'
                )
                self.add_error(
                    'end_date',
                    'Discontinuation date Should be Greater Than Launch'
                )
        return cleaned_data


class StateForm(forms.ModelForm):

    class Meta:
        model = State
        fields = ('__all__')


class TaxForm(forms.ModelForm):

    class Meta:
        model = TaxManager
        fields = ('__all__')


class DistrictForm(forms.ModelForm):

    class Meta:
        model = District
        fields = ('__all__')
        widgets = {
            'state':
            RelatedFieldWidgetCanAdd(
                related_model=State, url='/master/auto-complete/state/')
        }


class ProductGroupForm(forms.ModelForm):

    class Meta:
        model = ProductGroup
        fields = ('__all__')
        exclude = ('launch_discontinuation_date', 'tax')
        widgets = {
            'wef_date': MultipleRelatedFieldWidgetCanAdd(
                related_model=DatesManager,
                url='/master/auto-complete/date/'
            )
        }


class UnitForm(forms.ModelForm):

    class Meta:
        model = Unit
        fields = ('__all__')


class ProductForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.product_id = kwargs.pop('product_id', None)
        super(ProductForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Product
        fields = ('__all__')
        exclude = ('launch_discontinuation_date',)
        widgets = {
            'group': RelatedFieldWidgetCanAdd(
                related_model=ProductGroup,
                url='/master/auto-complete/product-group/'
            ),
            'unit':
            RelatedFieldWidgetCanAdd(
                related_model=Unit, url='/master/auto-complete/unit/'
            ),
        }

    def clean(self):
        cleaned_data = super().clean()

        product_group = cleaned_data.get('group')
        product_base_mrp = Product.objects.get(
            group=product_group,
            base_for_mrp=True
        ) if Product.objects.filter(
            group=product_group,
            base_for_mrp=True
        ).exists() else None

        if product_base_mrp and \
                product_base_mrp.id != self.product_id and \
                cleaned_data.get('base_for_mrp'):
            self.add_error(
                'base_for_mrp',
                product_base_mrp.name + " Is already MRP base for " +
                product_group.name
            )

        product_base_rate = Product.objects.get(
            group=product_group,
            base_for_rate=True
        ) if Product.objects.filter(
            group=product_group,
            base_for_rate=True
        ).exists() else None

        if product_base_rate and \
                product_base_rate.id != self.product_id and \
                cleaned_data.get('base_for_rate'):
            self.add_error(
                'base_for_rate',
                product_base_rate.name + " Is already Rate base for " +
                product_group.name
            )
        return cleaned_data


class DesignationForm(forms.ModelForm):

    class Meta:
        model = Designation
        fields = ('__all__')
        exclude = ('is_master',)
        widgets = {
            'reporting_designation':
            RelatedFieldWidgetCanAdd(
                related_model=Designation,
                url='/master/auto-complete/designation/'
            ),
        }


class EmployeeForm(forms.ModelForm):

    show_only_field_staff = forms.BooleanField(
        required=False,
        label="Show Distributor without Field Staff"
    )

    class Meta:
        model = Employee
        exclude = ('created_at', 'updated_at', 'bank', 'joining_leaving_date')
        fields = (
            'name', 'photo', 'address', 'state', 'district', 'town',
            'pin_code', 'personal_mobile_number', 'company_mobile_number',
            'email_id', 'blood_group', 'designation', 'call_code',
            'emergency_contact_name', 'emergency_contact_mobile_number',
            'emergency_contact_relation', 'pan_number',
            'pan_attachment', 'aadahar_number', 'aadahar_attachement', 'pf_no',
            'esic_no', 'bank_name', 'account_number', 'isfc_code', 'branch',
            'dob', 'cv', 'headquarter_state',
            'headquarter_district', 'headquarter_town', 'product_group',
            'show_only_field_staff', 'distributor', 'reporting_to'
        )
        widgets = {
            'state':
            RelatedFieldWidgetCanAdd(
                related_model=State, url='/master/auto-complete/state/'),
            'district':
            RelatedFieldWidgetCanAdd(
                related_model=District, url='/master/auto-complete/district/',
                forward=('state',)
            ),
            'town':
            RelatedFieldWidgetCanAdd(
                related_model=Town, url='/master/auto-complete/town/',
                forward=('district',)
            ),
            'designation':
            RelatedFieldWidgetCanAdd(
                related_model=Designation,
                url='/master/auto-complete/designation/'),
            'headquarter':
            RelatedFieldWidgetCanAdd(
                related_model=Town, url='/master/auto-complete/town/'),
            'reporting_to':
            RelatedFieldWidgetCanAdd(
                related_model=Employee, url='/master/auto-complete/employee/',
                forward=['designation', ]
            ),
            'product_group':
            MultipleRelatedFieldWidgetCanAdd(
                related_model=ProductGroup,
                url='/master/auto-complete/product-group/'
            ),
            'distributor':
            MultipleRelatedFieldWidgetCanAdd(
                related_model=Distributor,
                url='/master/auto-complete/distributor/',
                forward=(
                    forward.Const('employee', 'designation'),
                    'show_only_field_staff'
                )
            ),
            'headquarter_state':
            RelatedFieldWidgetCanAdd(
                related_model=State, url='/master/auto-complete/state/'),
            'headquarter_district':
            RelatedFieldWidgetCanAdd(
                related_model=District, url='/master/auto-complete/district/',
                forward=('headquarter_state',)
            ),
            'headquarter_town':
            RelatedFieldWidgetCanAdd(
                related_model=Town, url='/master/auto-complete/town/',
                forward=('headquarter_district',)
            ),
        }


class CompanyKeyPersonForm(forms.ModelForm):

    class Meta:
        model = CompanyKeyPerson
        fields = ('__all__')


class DistributorForm(forms.ModelForm):

    class Meta:
        model = Distributor
        fields = fields = (
            'code', 'firm_name', 'address', 'state', 'district', 'town',
            'pin_code', 'std_code', 'phone_number_1', 'phone_number_2',
            'fax_number',
            'fssai_number', 'fssai_attachement', 'gst_number',
            'gst_attachement', 'pan_number', 'pan_attachment',
            'bank_name', 'account_number', 'isfc_code', 'branch', 'weekly_off',
            'key_person', 'direct_from_company', 'local_selling',
            'onward_supply', 'hub', 'field_staff', 'product_group',
        )
        exclude = ('created_at', 'updated_at', 'bank')
        widgets = {
            'state':
            RelatedFieldWidgetCanAdd(
                related_model=State, url='/master/auto-complete/state/'),
            'district':
            RelatedFieldWidgetCanAdd(
                related_model=District, url='/master/auto-complete/district/',
                forward=('state',)
            ),
            'town':
            RelatedFieldWidgetCanAdd(
                related_model=Town, url='/master/auto-complete/town/',
                forward=('district',)
            ),
            'key_person':
            MultipleRelatedFieldWidgetCanAdd(
                related_model=CompanyKeyPerson,
                url='/master/auto-complete/company-key-person/'
            ),
            'product_group':
            MultipleRelatedFieldWidgetCanAdd(
                related_model=ProductGroup,
                url='/master/auto-complete/product-group/'
            ),
            'hub':
            MultipleRelatedFieldWidgetCanAdd(
                related_model=Distributor,
                url='/master/auto-complete/distributor/',
                forward=(forward.Const('key_person', 'hub'),)
            ),
            'field_staff':
            RelatedFieldWidgetCanAdd(
                related_model=Employee,
                url='/master/auto-complete/employee/',
                forward=(forward.Const('field_staff', 'field_staff'),)
            )
        }

    def clean(self):
        cleaned_data = super().clean()

        local_selling = cleaned_data.get('local_selling')
        onward_supply = cleaned_data.get('onward_supply')

        if not (local_selling or onward_supply):
            self.add_error(
                'local_selling',
                'Either local selling or onward supply is required'
            )
            self.add_error(
                'onward_supply',
                'Either local selling or onward supply is required'
            )
        return cleaned_data
