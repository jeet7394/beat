function openModel(url) {
    $.get(url, function(data, status) {
        var parser = new DOMParser();
        doc = parser.parseFromString(data, "text/html");
        doc = doc.querySelector(".uk-container");

        var form = doc.querySelector("form");

        document.querySelector("#modal-base .uk-modal-dialog").innerHTML =  `<iframe height="100%" width="100%" frameborder="0" style="overflow:hidden;height:600px;width:100%" style="padding:10px" src="${url}"></iframe>`;

        window.modal = UIkit.modal("#modal-base").show();
    });
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function handleModelFormSubmit(url) {
    var formObj = {};
    var data = new FormData($('#abacus')[0]);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        beforeSend: function(request) {
            request.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        },
        success: function (data) {
            alert("Added");
        },
        error: function (e) {
        }
    });
    return false;
}

// $(document).ready(function(){
//     $(".my_required").each(function(index, input){
        
//         if (input.type === "select-one" || input.type === "select-multiple") {
//             console.log(input)
//             input.parentElement.parentElement.querySelector("label").innerHTML += " *"
//         }
//         else {
//             input.parentElement.querySelector("label").innerHTML += " *"
//         }
//     });
// });
