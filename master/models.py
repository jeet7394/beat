from django.db import models
from django.utils import timezone
from django.core.validators import validate_email, RegexValidator
from django.utils.safestring import mark_safe


class DatesManager(models.Model):
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)

    def __str__(self):
        if self.end_date:
            return self.start_date.strftime("%d-%b-%Y") \
                + " to " + self.end_date.strftime("%d-%b-%Y")
        else:
            return self.start_date.strftime("%d-%b-%Y") \
                + " to None"


class TaxManager(models.Model):
    tax_rate = models.FloatField()
    wef_date = models.DateField()

    def __str__(self):
        return str(self.tax_rate) + " on " + self.wef_date.strftime("%d-%b-%Y")


class State(models.Model):
    """
    State Table
    """
    name = models.CharField(max_length=255, unique=True)
    code = models.CharField(max_length=2, unique=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.name


class District(models.Model):
    """
    District Table
    """
    name = models.CharField(max_length=255)
    state = models.ForeignKey(State, models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.name + ', ' + self.state.name


class Town(models.Model):
    """
    Town Table
    """
    name = models.CharField(max_length=255)
    state = models.ForeignKey(State, on_delete=models.PROTECT)
    district = models.ForeignKey(District, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.name


class Unit(models.Model):
    """
    Unit Table
    """
    code = models.CharField(max_length=3, unique=True)
    name = models.CharField(max_length=255, unique=True)
    decimal_places = models.CharField(max_length=255)
    weight_of_single_unit = models.DecimalField(
        default=0,
        max_digits=10,
        decimal_places=3
    )
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.name


class ProductGroup(models.Model):
    """
    Product Table
    """
    code = models.CharField(max_length=4, unique=True)
    name = models.CharField(max_length=255)
    manual_rate = models.BooleanField(default=False)
    manual_MRP = models.BooleanField(default=False)
    enable_SPD = models.BooleanField(default=False)
    launch_discontinuation_date = models.ManyToManyField(
        DatesManager,
        blank=True,
        related_name="l_d_date"
    )
    tax = models.ManyToManyField(TaxManager, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.name

    @property
    def launch_dates(self):
        return mark_safe(
            ',<br/>'.join(
                [str(date) for date in self.launch_discontinuation_date.all()]
            )
        )

    @property
    def tax_wef(self):
        return mark_safe(',<br/>'.join([str(tax) for tax in self.tax.all()]))


class Product(models.Model):
    name = models.CharField(max_length=255)
    group = models.ForeignKey(ProductGroup, on_delete=models.PROTECT)
    short_code = models.CharField(max_length=10)
    unit = models.ForeignKey(Unit, models.PROTECT)
    content = models.CharField(max_length=255)
    current_case_size = models.IntegerField(default=0)
    packaging_charges = models.IntegerField(default=0)
    launch_discontinuation_date = models.ManyToManyField(
        DatesManager,
        blank=True
    )

    base_for_mrp = models.BooleanField(default=False)
    mrp_round = models.CharField(max_length=255, choices=(
        ("2 Decimals", "2 Decimals"),
        ("Nearest Upper", "Nearest Upper"),
        ("Nearest Lower", "Nearest Lower"),
    ), default="2 Decimals")
    default_mrp = models.FloatField(default=0.0)

    base_for_rate = models.BooleanField(default=False)
    rate_round = models.CharField(max_length=255, choices=(
        ("2 Decimals", "2 Decimals"),
        ("Nearest Upper", "Nearest Upper"),
        ("Nearest Lower", "Nearest Lower"),
    ), default="2 Decimals")
    default_rate = models.FloatField(default=0.0)

    formula_for_mrp = models.TextField(blank=True)
    formula_for_rate = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.name


class Designation(models.Model):
    code = models.CharField(max_length=3, unique=True)
    name = models.CharField(max_length=255, unique=True)
    work_area = models.CharField(max_length=255, choices=(
        ('office', 'Office'),
        ('field', 'Field'),
        ('both', 'Both')
    ))
    reporting_designation = models.ForeignKey(
        "Designation",
        models.PROTECT,
        blank=True,
        null=True
    )
    is_master = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.name


def upload_user_photo_to(instance, filename):
    return "images/{0}/{1}".format(instance.name, filename)


def upload_user_document_to(instance, filename):
    return "documents/employee/{0}/{1}".format(instance.name, filename)


class Employee(models.Model):
    name = models.CharField(max_length=255)
    photo = models.ImageField(
        upload_to=upload_user_photo_to,
        blank=True,
        null=True
    )
    address = models.TextField()
    state = models.ForeignKey(State, models.PROTECT)
    district = models.ForeignKey(District, models.PROTECT)
    town = models.ForeignKey(
        Town,
        models.PROTECT,
        related_name="town_name"
    )
    pin_code = models.IntegerField(
        validators=[
            RegexValidator(
                regex=r'^[0-9]{6}',
                message="Pincode should consist of 6 digits"
            )
        ]
    )
    personal_mobile_number = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        validators=[
            RegexValidator(
                regex=r'^[0-9]{10}$',
                message="Phone Number should be of 10 digits",
                code="invalid_phone_number_1"
            )
        ]
    )
    company_mobile_number = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        validators=[
            RegexValidator(
                regex=r'^[0-9]{10}$',
                message="Phone Number should be of 10 digits",
                code="invalid_phone_number_2"
            )
        ]
    )
    email_id = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        validators=[
            validate_email
        ]
    )
    blood_group = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        choices=(
            ('A+', 'A+'),
            ('B+', 'B+'),
            ('O+', 'O+'),
            ('AB+', 'AB+'),
            ('A-', 'A-'),
            ('B-', 'B-'),
            ('O-', 'O-'),
            ('AB-', 'AB-')
        )
    )
    designation = models.ForeignKey(Designation, models.PROTECT)
    call_code = models.CharField(
        max_length=4,
        unique=True
    )
    # Emergency
    emergency_contact_name = models.CharField(max_length=255)
    emergency_contact_mobile_number = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        validators=[
            RegexValidator(
                regex=r'^[0-9]{10}$',
                message="Phone Number should be of 10 digits",
                code="invalid_phone_number_2"
            )
        ]
    )
    emergency_contact_relation = models.CharField(max_length=255)

    pan_number = models.CharField(max_length=10, validators=[
        RegexValidator(
            regex=r'^[A-Z]{3}[P][A-Z]{1}[0-9]{4}[A-Z]{1}$',
            message="Invalid Pan Number",
            code="invalid_pan"
        )
    ])
    pan_attachment = models.ImageField(
        upload_to=upload_user_document_to,
        blank=True,
        null=True
    )

    aadahar_number = models.CharField(max_length=14, validators=[
        RegexValidator(
            regex=r'^\d{4}\d{4}\d{4}$',
            message="Invalid Aadahar Card",
            code="invalid_aadhar"
        )
    ])
    aadahar_attachement = models.ImageField(
        upload_to=upload_user_document_to,
        blank=True,
        null=True
    )

    pf_no = models.CharField(max_length=255, blank=True, null=True)
    esic_no = models.CharField(max_length=255, blank=True, null=True)

    bank_name = models.CharField(max_length=255)
    account_number = models.CharField(max_length=255)
    isfc_code = models.CharField(max_length=255)
    branch = models.CharField(max_length=255)

    dob = models.DateField()
    joining_leaving_date = models.ManyToManyField(
        DatesManager,
        blank=True
    )
    cv = models.ImageField(
        upload_to=upload_user_document_to,
        blank=True,
        null=True
    )
    # Headquerter
    headquarter_state = models.ForeignKey(
        State,
        models.PROTECT,
        related_name="headquarter_state"
    )
    headquarter_district = models.ForeignKey(
        District,
        models.PROTECT,
        related_name="headquarter_district"
    )
    headquarter_town = models.ForeignKey(
        Town,
        models.PROTECT,
        related_name="headquarter_town"
    )

    product_group = models.ManyToManyField(ProductGroup)
    distributor = models.ManyToManyField(
        "Distributor",
        related_name="distributor_name"
    )
    reporting_to = models.ForeignKey(
        "Employee",
        models.PROTECT,
        related_name="reporting_to_name",
        blank=True,
        null=True
    )
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.name

    @property
    def launch_dates(self):
        return mark_safe(
            ',<br/>'.join(
                [str(date) for date in self.joining_leaving_date.all()]
            )
        )


def upload_company_document_to(instance, filename):
    return "documents/company/{instance.firm_name}/{filename}"


class CompanyKeyPerson(models.Model):
    name = models.CharField(max_length=255)
    designation = models.CharField(choices=(
        ('Proprietor', 'Proprietor'),
        ('Director', 'Director'),
        ('Partner', 'Partner'),
        ('Staffmember', 'Staffmember')
    ), max_length=255)
    email_id = models.CharField(
        max_length=255,
        blank=True,
        null=True
    )
    mobile_number = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        validators=[
            RegexValidator(
                regex=r'^[0-9]{10}$',
                message="Phone Number should be of 10 digits",
                code="invalid_phone_number_2"
            )
        ]
    )
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.name


class Company(models.Model):
    code = models.CharField(max_length=5, unique=True)
    firm_name = models.CharField(max_length=255)
    address = models.TextField()
    state = models.ForeignKey(State, models.PROTECT)
    district = models.ForeignKey(District, models.PROTECT)
    town = models.ForeignKey(Town, models.PROTECT)
    pin_code = models.CharField(max_length=6, validators=[
        RegexValidator(
            regex=r'^[0-9]{6}$',
            message="Pincode should be of 6 digits",
            code="invalid_pincode"
        )
    ])
    std_code = models.IntegerField(default=0)
    phone_number_1 = models.CharField(max_length=10, validators=[
        RegexValidator(
            regex=r'^[0-9]{10}$',
            message="Phone Number should be of 10 digits",
            code="invalid_phone_number_1"
        )
    ])
    phone_number_2 = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        validators=[
            RegexValidator(
                regex=r'^[0-9]{10}$',
                message="Phone Number should be of 10 digits",
                code="invalid_phone_number_2"
            )
        ]
    )

    fssai_number = models.IntegerField(blank=True, null=True)
    fssai_attachement = models.ImageField(
        upload_to=upload_company_document_to,
        blank=True,
        null=True
    )

    gst_number = models.CharField(
        max_length=255,
        validators=[
            RegexValidator(
                regex=r'^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$'
            )
        ]
    )
    gst_attachement = models.ImageField(
        upload_to=upload_company_document_to,
        blank=True,
        null=True
    )

    pan_number = models.CharField(max_length=10, validators=[
        RegexValidator(
            regex=r'^[A-Z]{4}[A-Z]{1}[0-9]{4}[A-Z]{1}$',
            message="Invalid Pan Number (XXXPX1234X)",
            code="invalid_pan"
        )
    ])
    pan_attachment = models.ImageField(
        upload_to=upload_user_document_to,
        blank=True,
        null=True
    )

    bank_name = models.CharField(max_length=255)
    account_number = models.CharField(max_length=255)
    isfc_code = models.CharField(max_length=255)
    branch = models.CharField(max_length=255)

    appointing_cease_date = models.ManyToManyField(
        DatesManager,
        blank=True
    )
    key_person = models.ManyToManyField(CompanyKeyPerson)
    product_group = models.ManyToManyField(
        ProductGroup,
        blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.firm_name


def upload_distributor_document_to(instance, filename):
    return "documents/distributor/{instance.firm_name}/{filename}"


class Distributor(models.Model):
    code = models.CharField(max_length=5, unique=True)
    firm_name = models.CharField(max_length=255)
    address = models.TextField()
    state = models.ForeignKey(State, models.PROTECT)
    district = models.ForeignKey(District, models.PROTECT)
    town = models.ForeignKey(Town, models.PROTECT)

    pin_code = models.CharField(max_length=6, validators=[
        RegexValidator(
            regex=r'^[0-9]{6}$',
            message="Pincode should be of 6 digits",
            code="invalid_pincode"
        )
    ])
    std_code = models.IntegerField(default=0)

    phone_number_1 = models.CharField(max_length=10, validators=[
        RegexValidator(
            regex=r'^[0-9]{10}$',
            message="Phone Number should be of 10 digits",
            code="invalid_phone_number_1"
        )
    ])
    phone_number_2 = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        validators=[
            RegexValidator(
                regex=r'^[0-9]{10}$',
                message="Phone Number should be of 10 digits",
                code="invalid_phone_number_2"
            )
        ]
    )

    fax_number = models.CharField(max_length=10)

    fssai_number = models.IntegerField()
    fssai_attachement = models.ImageField(
        upload_to=upload_company_document_to,
        blank=True,
        null=True
    )

    gst_number = models.CharField(
        max_length=255,
        validators=[
            RegexValidator(
                regex=r'^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$',
                message="Invalid GST Number (NNCCCCCNNNNCXZX)",
            )
        ]
    )
    gst_attachement = models.ImageField(
        upload_to=upload_company_document_to,
        blank=True,
        null=True
    )

    pan_number = models.CharField(max_length=10, validators=[
        RegexValidator(
            regex=r'^[A-Z]{4}[A-Z]{1}[0-9]{4}[A-Z]{1}$',
            message="Invalid Pan Number (XXXPX1234X)",
            code="invalid_pan"
        )
    ])
    pan_attachment = models.ImageField(
        upload_to=upload_user_document_to,
        blank=True,
        null=True
    )

    bank_name = models.CharField(max_length=255)
    account_number = models.CharField(max_length=255)
    isfc_code = models.CharField(max_length=255)
    branch = models.CharField(max_length=255)

    appointing_cease_date = models.ManyToManyField(
        DatesManager,
        blank=True
    )

    weekly_off = models.CharField(max_length=255, choices=(
        ('Monday', 'Monday'),
        ('Tuesday', 'Tuesday'),
        ('Wednesday', 'Wednesday'),
        ('Thursday', 'Thursday'),
        ('Friday', 'Friday'),
        ('Saturday', 'Saturday'),
        ('Sunday', 'Sunday')
    ), blank=True, null=True)

    key_person = models.ManyToManyField(CompanyKeyPerson)
    direct_from_company = models.BooleanField(default=False)
    local_selling = models.NullBooleanField()
    onward_supply = models.NullBooleanField()
    product_group = models.ManyToManyField(ProductGroup, blank=True)
    hub = models.ManyToManyField('self', blank=True)
    field_staff = models.ForeignKey(
        Employee,
        models.PROTECT,
        "distributor_field_staff",
        blank=True,
        null=True
    )
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.firm_name


class Transporter(models.Model):
    name = models.CharField(max_length=255)
    address = models.TextField()

    gst_number = models.CharField(
        max_length=255,
        validators=[
            RegexValidator(
                regex=r'^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$'
            )
        ]
    )
    gst_attachement = models.ImageField(
        upload_to=upload_company_document_to,
        blank=True,
        null=True
    )

    pan_number = models.CharField(max_length=10, validators=[
        RegexValidator(
            regex=r'^[A-Z]{4}[A-Z]{1}[0-9]{4}[A-Z]{1}$',
            message="Invalid Pan Number (XXXPX1234X)",
            code="invalid_pan"
        )
    ])
    pan_attachment = models.ImageField(
        upload_to=upload_user_document_to,
        blank=True,
        null=True
    )

    tds_rate = models.FloatField()
    owner_name = models.CharField(max_length=255)
    mobile_number_1 = models.CharField(max_length=10, validators=[
        RegexValidator(
            regex=r'^[0-9]{10}$',
            message="Phone Number should be of 10 digits",
            code="invalid_phone_number_1"
        )
    ])
    mobile_number_2 = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        validators=[
            RegexValidator(
                regex=r'^[0-9]{10}$',
                message="Phone Number should be of 10 digits",
                code="invalid_phone_number_2"
            )
        ]
    )
    email_id = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        validators=[
            validate_email
        ]
    )

    def __str__(self):
        return self.name
