def generate_date_dict(obj, accessor):
    launch_date = getattr(obj, accessor).all().order_by('start_date')
    date_dict = {
        "dates-TOTAL_FORMS": str(launch_date.count()),
        "dates-INITIAL_FORMS": "0",
        "dates-MIN_NUM_FORMS": "0",
        "dates-MAX_NUM_FORMS": "1000",
    }

    count = 0
    for each_date in launch_date:
        date_dict["dates-"+str(count)+"-start_date"] \
            = each_date.start_date.strftime("%Y-%m-%d") \
            if each_date.start_date is not None else ''
        date_dict["dates-"+str(count)+"-end_date"] \
            = each_date.end_date.strftime("%Y-%m-%d") \
            if each_date.end_date is not None else ''
        count += 1

    return date_dict


def generate_tax_dict(obj, accessor):
    tax = getattr(obj, accessor).all()
    tax_dict = {
        "tax-TOTAL_FORMS": str(tax.count()),
        "tax-INITIAL_FORMS": "0",
        "tax-MIN_NUM_FORMS": "0",
        "tax-MAX_NUM_FORMS": "1000",
    }

    count = 0

    for each_tax in tax:
        tax_dict["tax-"+str(count)+"-tax_rate"] = each_tax.tax_rate
        tax_dict["tax-"+str(count)+"-wef_date"] = each_tax.wef_date
        count += 1

    return tax_dict


def generate_key_person_dict(obj, accessor):
    key_person = getattr(obj, accessor).all()
    key_person_dict = {
        "key_person-TOTAL_FORMS": str(key_person.count()),
        "key_person-INITIAL_FORMS": "0",
        "key_person-MIN_NUM_FORMS": "0",
        "key_person-MAX_NUM_FORMS": "1000",
    }

    count = 0

    for each_tax in key_person:
        key_person_dict["key_person-"+str(count)+"-name"] = each_tax.name
        key_person_dict["key_person-"+str(count)+"-designation"] = each_tax.designation
        key_person_dict["key_person-"+str(count)+"-email_id"] = each_tax.email_id
        key_person_dict["key_person-"+str(count)+"-mobile_number"] = each_tax.mobile_number
        count += 1

    return key_person_dict
