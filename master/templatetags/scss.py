from os import path
from django import template
from django.conf import settings
from sass import compile
from django.utils.safestring import mark_safe

register = template.Library()

@register.simple_tag
def scss(file_path):
    static_dirs = settings.STATICFILES_DIRS
    contents = ''
    for each_dir in static_dirs:
        file = path.join(each_dir, file_path)
        if path.isfile(file):
            with open(file, 'r+') as f:
                for line in f.readlines():
                    contents += line
    compiles_scss = compile(string=contents, output_style='compressed')
    return mark_safe("<style>{0}</style>".format(compiles_scss))
