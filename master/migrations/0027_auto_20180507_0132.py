# Generated by Django 2.0.3 on 2018-05-06 20:02

import django.core.validators
from django.db import migrations, models
import master.models


class Migration(migrations.Migration):

    dependencies = [
        ('master', '0026_auto_20180506_2210'),
    ]

    operations = [
        migrations.CreateModel(
            name='Transporter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('address', models.TextField()),
                ('gst_number', models.CharField(max_length=255, validators=[django.core.validators.RegexValidator(regex='^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$')])),
                ('gst_attachement', models.ImageField(blank=True, null=True, upload_to=master.models.upload_company_document_to)),
                ('pan_number', models.CharField(max_length=10, validators=[django.core.validators.RegexValidator(code='invalid_pan', message='Invalid Pan Number (XXXPX1234X)', regex='^[A-Z]{4}[A-Z]{1}[0-9]{4}[A-Z]{1}$')])),
                ('pan_attachment', models.ImageField(blank=True, null=True, upload_to=master.models.upload_user_document_to)),
                ('tds_rate', models.FloatField()),
                ('owner_name', models.CharField(max_length=255)),
                ('mobile_number_1', models.CharField(max_length=10, validators=[django.core.validators.RegexValidator(code='invalid_phone_number_1', message='Phone Number should be of 10 digits', regex='^[0-9]{10}$')])),
                ('mobile_number_2', models.CharField(blank=True, max_length=10, null=True, validators=[django.core.validators.RegexValidator(code='invalid_phone_number_2', message='Phone Number should be of 10 digits', regex='^[0-9]{10}$')])),
                ('email_id', models.CharField(blank=True, max_length=255, null=True, validators=[django.core.validators.EmailValidator()])),
            ],
        ),
        migrations.AddField(
            model_name='distributor',
            name='direct_from_company',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='distributor',
            name='gst_number',
            field=models.CharField(max_length=255, validators=[django.core.validators.RegexValidator(regex='^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$')]),
        ),
        migrations.AlterField(
            model_name='distributor',
            name='phone_number_1',
            field=models.CharField(max_length=10, validators=[django.core.validators.RegexValidator(code='invalid_phone_number_1', message='Phone Number should be of 10 digits', regex='^[0-9]{10}$')]),
        ),
        migrations.AlterField(
            model_name='distributor',
            name='phone_number_2',
            field=models.CharField(blank=True, max_length=10, null=True, validators=[django.core.validators.RegexValidator(code='invalid_phone_number_2', message='Phone Number should be of 10 digits', regex='^[0-9]{10}$')]),
        ),
        migrations.AlterField(
            model_name='distributor',
            name='pin_code',
            field=models.CharField(max_length=6, validators=[django.core.validators.RegexValidator(code='invalid_pincode', message='Pincode should be of 6 digits', regex='^[0-9]{6}$')]),
        ),
        migrations.AlterField(
            model_name='distributor',
            name='std_code',
            field=models.IntegerField(default=0),
        ),
    ]
