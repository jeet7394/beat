import django_tables2 as tables
from django_tables2.utils import A
from django.utils.safestring import mark_safe
from master import models


class TownTable(tables.Table):

    name = tables.LinkColumn('edit_town', args=[A('pk')])
    delete = tables.LinkColumn(
        'delete',
        args=['Town', A('pk')],
        text=mark_safe('<span class="delete-icon" uk-icon="trash"></span>'),
        orderable=False
    )

    class Meta:
        model = models.Town
        attrs = {'class': 'uk-table uk-table-striped uk-table-divider'}
        exclude = ('created_at', 'updated_at')


class CompanyTable(tables.Table):

    firm_name = tables.LinkColumn('edit_company', args=[A('pk')])
    delete = tables.LinkColumn(
        'delete',
        args=['Company', A('pk')],
        text=mark_safe('<span class="delete-icon" uk-icon="trash"></span>'),
        orderable=False
    )

    class Meta:
        model = models.Company
        attrs = {'class': 'uk-table uk-table-striped uk-table-divider'}
        # fields = ('__all__',)
        exclude = ('created_at', 'updated_at')


class DatesManagerTable(tables.Table):

    class Meta:
        model = models.DatesManager
        attrs = {'class': 'uk-table uk-table-striped uk-table-divider'}
        fields = ['id', 'date']


class StateTable(tables.Table):

    name = tables.LinkColumn('edit_state', args=[A('pk')])
    delete = tables.LinkColumn(
        'delete',
        args=['State', A('pk')],
        text=mark_safe('<span class="delete-icon" uk-icon="trash"></span>'),
        orderable=False
    )

    class Meta:
        model = models.State
        attrs = {'class': 'uk-table uk-table-striped uk-table-divider'}
        fields = ['id', 'name', "code"]


class DistrictTable(tables.Table):

    name = tables.LinkColumn('edit_district', args=[A('pk')])
    delete = tables.LinkColumn(
        'delete',
        args=['District', A('pk')],
        text=mark_safe('<span class="delete-icon" uk-icon="trash"></span>'),
        orderable=False
    )

    class Meta:
        model = models.District
        attrs = {'class': 'uk-table uk-table-striped uk-table-divider'}
        fields = ['id', 'name', "state"]


class UnitTable(tables.Table):

    name = tables.LinkColumn('edit_unit', args=[A('pk')])
    delete = tables.LinkColumn(
        'delete',
        args=['Unit', A('pk')],
        text=mark_safe('<span class="delete-icon" uk-icon="trash"></span>'),
        orderable=False
    )

    class Meta:
        model = models.Unit
        attrs = {'class': 'uk-table uk-table-striped uk-table-divider'}
        fields = [
            'id', 'name', "code", "decimal_places", "weight_of_single_unit"
        ]


class ProductGroupTable(tables.Table):
    launch_dates = tables.Column(
        accessor='launch_dates',
        verbose_name='Launch Dates'
    )

    tax = tables.Column(
        accessor='tax_wef',
        verbose_name='Tax'
    )

    name = tables.LinkColumn('edit_pg', args=[A('pk')])
    delete = tables.LinkColumn(
        'delete',
        args=['ProductGroup', A('pk')],
        text=mark_safe('<span class="delete-icon" uk-icon="trash"></span>'),
        orderable=False
    )

    class Meta:
        model = models.ProductGroup
        attrs = {'class': 'uk-table uk-table-striped uk-table-divider'}
        exclude = ('created_at', 'updated_at')


class ProductTable(tables.Table):

    name = tables.LinkColumn("edit_product", args=[A('pk')])
    delete = tables.LinkColumn(
        'delete',
        args=['Product', A('pk')],
        text=mark_safe('<span class="delete-icon" uk-icon="trash"></span>'),
        orderable=False
    )

    class Meta:
        model = models.Product
        attrs = {'class': 'uk-table uk-table-striped uk-table-divider'}
        exclude = ('created_at', 'updated_at')


class DesignationTable(tables.Table):

    name = tables.LinkColumn('edit_designation', args=[A('pk')])
    delete = tables.LinkColumn(
        'delete',
        args=['Designation', A('pk')],
        text=mark_safe('<span class="delete-icon" uk-icon="trash"></span>'),
        orderable=False
    )

    class Meta:
        model = models.Designation
        attrs = {'class': 'uk-table uk-table-striped uk-table-divider'}
        exclude = ('created_at', 'updated_at')


class EmployeeTable(tables.Table):

    launch_dates = tables.Column(
        accessor='launch_dates',
        verbose_name='Appointment And Leaving Dates'
    )

    name = tables.LinkColumn('edit_employee', args=[A('pk')])
    delete = tables.LinkColumn(
        'delete',
        args=['Employee', A('pk')],
        text=mark_safe('<span class="delete-icon" uk-icon="trash"></span>'),
        orderable=False
    )

    class Meta:
        model = models.Employee
        attrs = {'class': 'uk-table uk-table-striped uk-table-divider'}
        exclude = ('created_at', 'updated_at', 'bank')


class CompanyKeyPersonTable(tables.Table):

    class Meta:
        model = models.CompanyKeyPerson
        attrs = {'class': 'uk-table uk-table-striped uk-table-divider'}
        exclude = ('created_at', 'updated_at')


class DistributorTable(tables.Table):

    firm_name = tables.LinkColumn('edit_distributor', args=[A('pk')])
    delete = tables.LinkColumn(
        'delete',
        args=['Distributor', A('pk')],
        text=mark_safe('<span class="delete-icon" uk-icon="trash"></span>'),
        orderable=False
    )

    class Meta:
        model = models.Distributor
        attrs = {'class': 'uk-table uk-table-striped uk-table-divider'}
        # fields = ('__all__',)
        exclude = ('created_at', 'updated_at')
