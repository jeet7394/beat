from django.shortcuts import render, redirect
from django.views import View
from django.contrib import messages
from django.forms import formset_factory
from django_tables2 import RequestConfig
from django_tables2.export.export import TableExport
from master.forms import TownForm, CompanyForm, StateForm, DistrictForm, \
    ProductGroupForm, ProductForm, DesignationForm, EmployeeForm,\
    CompanyKeyPersonForm, DistributorForm, UnitForm, DatesManagerForm, \
    TaxForm, BaseDatesFormSet, BaseTaxFormSet
from master import models
from master import tables
from master.admin import required_model
from master import filters


class TownView(View):

    def get(self, request):
        return self.add(request) if 'add' in request.GET \
            else self.display(request)

    def display(self, request):
        town_filter = filters.TownFilter(request.GET)
        town_table = tables.TownTable(town_filter.qs)
        RequestConfig(request, paginate={'per_page': 50}).configure(town_table)
        context = {'table': town_table, 'filter': town_filter}

        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            exporter = TableExport(export_format, town_table)
            return exporter.response('table.{}'.format(export_format))

        return render(request, 'town/display_town.html', context)

    def add(self, request):
        form = TownForm()
        return render(request, 'town/add_town.html', {'form': form})

    def post(self, request):
        form = TownForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS,
                                 'Town ' + form.data['name'] + ' Added')
            return redirect("/master")
        return render(request, 'town/add_town.html', {'form': form})


class StateView(View):

    def get(self, request):
        return self.add(request) if 'add' in request.GET \
            else self.display(request)

    def display(self, request):
        state_filter = filters.StateFilter(request.GET)
        state_table = tables.StateTable(state_filter.qs)
        RequestConfig(request, paginate={'per_page': 50}).configure(state_table)
        context = {'table': state_table, 'filter': state_filter}

        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            exporter = TableExport(export_format, state_table)
            return exporter.response('table.{}'.format(export_format))

        return render(request, 'state/display_state.html', context)

    def add(self, request):
        form = StateForm()
        return render(request, 'state/add_state.html', {'form': form})

    def post(self, request):
        form = StateForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS,
                                 'State ' + form.data['name'] + ' Added')
            return redirect("/master")
        return render(request, 'state/add_state.html', {'form': form})


class DistrictView(View):

    def get(self, request):
        return self.add(request) if 'add' in request.GET\
            else self.display(request)

    def display(self, request):
        district_filter = filters.DistrictFilter(request.GET)
        district_table = tables.DistrictTable(district_filter.qs)
        RequestConfig(
            request, paginate={
                'per_page': 50
            }).configure(district_table)
        context = {'table': district_table, 'filter': district_filter}

        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            exporter = TableExport(export_format, district_table)
            return exporter.response('table.{}'.format(export_format))

        return render(request, 'district/display_district.html', context)

    def add(self, request):
        form = DistrictForm()
        return render(request, 'district/add_district.html', {'form': form})

    def post(self, request):
        form = DistrictForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS,
                                 'District ' + form.data['name'] + ' Added')
            return redirect("/master")
        return render(request, 'district/add_district.html', {'form': form})


class ProductGroupView(View):

    def get(self, request):
        return self.add(request) if 'add' in request.GET\
            else self.display(request)

    def display(self, request):
        product_group_filter = filters.ProductGroupFilter(request.GET)
        product_group_table = tables.ProductGroupTable(product_group_filter.qs)
        RequestConfig(
            request, paginate={
                'per_page': 50
            }).configure(product_group_table)
        context = {
            'table': product_group_table,
            'filter': product_group_filter
        }

        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            exporter = TableExport(export_format, product_group_table)
            return exporter.response('table.{}'.format(export_format))

        return render(request, 'product_group/display_product_group.html',
                      context)

    def add(self, request):
        form = ProductGroupForm()
        dates_form = formset_factory(
            DatesManagerForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseDatesFormSet
        )(prefix='dates')
        tax_form = formset_factory(
            TaxForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseTaxFormSet
        )(prefix='tax')
        return render(
            request,
            'product_group/add_product_group.html',
            {
                'form': form,
                'date_form': dates_form,
                'tax_form': tax_form
            }
        )

    def post(self, request):
        form = ProductGroupForm(request.POST)
        dates_form = formset_factory(
            DatesManagerForm,
            extra=1,
            formset=BaseDatesFormSet
        )(request.POST, prefix='dates')
        tax_form = formset_factory(
            TaxForm,
            extra=1,
            formset=BaseTaxFormSet
        )(request.POST, prefix='tax')
        form_error = False
        if form.is_valid() and tax_form.is_valid() and dates_form.is_valid():
            for tax in tax_form:
                if not tax.is_valid():
                    form_error = True
            for date in dates_form:
                if not date.is_valid():
                    form_error = True
            if not form_error:
                p_g = form.save()

                for tax in tax_form:
                    new_tax = tax.save()
                    p_g.tax.add(new_tax)

                for date in dates_form:
                    new_date = date.save()
                    p_g.launch_discontinuation_date.add(new_date)

                messages.add_message(request, messages.SUCCESS,
                                    'ProductGroup ' + form.data['name'] + ' Added')
                return redirect("/master")
        return render(
            request,
            'product_group/add_product_group.html', {
                'form': form,
                'date_form': dates_form,
                'tax_form': tax_form
            }
        )


class ProductView(View):

    def get(self, request):
        return self.add(request) if 'add' in request.GET\
            else self.display(request)

    def display(self, request):
        product_filter = filters.ProductFilter(request.GET)
        product_table = tables.ProductTable(product_filter.qs)
        RequestConfig(
            request, paginate={
                'per_page': 50
            }).configure(product_table)
        context = {'table': product_table, 'filter': product_filter}

        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            exporter = TableExport(export_format, product_table)
            return exporter.response('table.{}'.format(export_format))

        return render(request, 'product/display_product.html', context)

    def add(self, request):
        form = ProductForm()
        dates_form = formset_factory(
            DatesManagerForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseDatesFormSet
        )(prefix='dates')
        return render(
            request,
            'product/add_product.html',
            {
                'form': form,
                'date_form': dates_form
            }
        )

    def post(self, request):
        form = ProductForm(request.POST, request.FILES)
        dates_form = formset_factory(
            DatesManagerForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseDatesFormSet,
        )(request.POST, prefix='dates')
        form_error = False

        if form.is_valid() and dates_form.is_valid():
            for date in dates_form:
                if not date.is_valid():
                    form_error = True

            if not form_error:
                product = form.save()
                product.save()

                for date in dates_form:
                    new_date = date.save()
                    product.launch_discontinuation_date.add(new_date)
                return redirect('/master/')
        return render(request, 'product/add_product.html', {'form': form})


class DesignationView(View):

    def get(self, request):
        return self.add(request) if 'add' in request.GET\
            else self.display(request)

    def display(self, request):
        designation_filter = filters.DesignationFilter(request.GET)
        designation_table = tables.DesignationTable(designation_filter.qs)
        RequestConfig(
            request, paginate={
                'per_page': 50
            }).configure(designation_table)
        context = {'table': designation_table, 'filter': designation_filter}

        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            exporter = TableExport(export_format, designation_table)
            return exporter.response('table.{}'.format(export_format))

        return render(request, 'designation/display_designation.html', context)

    def add(self, request):
        form = DesignationForm()
        return render(request, 'designation/add_designation.html',
                      {'form': form})

    def post(self, request):
        form = DesignationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS,
                                 'Designation ' + form.data['name'] + ' Added')
            return redirect("/master")
        return render(request, 'designation/add_designation.html', {'form': form})


class EmployeeView(View):

    def get(self, request):
        return self.add(request) if 'add' in request.GET\
            else self.display(request)

    def display(self, request):
        employee_filter = filters.EmployeeFilter(request.GET)
        employee_table = tables.EmployeeTable(employee_filter.qs)
        RequestConfig(
            request, paginate={
                'per_page': 50
            }).configure(employee_table)
        context = {'table': employee_table, 'filter': employee_filter}

        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            exporter = TableExport(export_format, employee_table)
            return exporter.response('table.{}'.format(export_format))

        return render(request, 'employee/display_employee.html', context)

    def add(self, request):
        form = EmployeeForm()
        dates_form = formset_factory(
            DatesManagerForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseDatesFormSet
        )(prefix='dates')
        return render(request, 'employee/add_employee.html', {
            'form': form,
            'date_form': dates_form
        })

    def post(self, request):
        form = EmployeeForm(request.POST, request.FILES)
        dates_form = formset_factory(
            DatesManagerForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseDatesFormSet,
        )(request.POST, prefix='dates')
        form_error = False

        if form.is_valid() and dates_form.is_valid():
            for date in dates_form:
                if not date.is_valid():
                    form_error = True

            if not form_error:
                employee = form.save()

                for date in dates_form:
                    new_date = date.save()
                    employee.joining_leaving_date.add(new_date)
                return redirect('/master/')

        return render(request, 'employee/add_employee.html', {
            'form': form,
            'date_form': dates_form
        })


class CompanyView(View):

    def get(self, request):
        return self.add(request) if 'add' in request.GET \
            else self.display(request)

    def display(self, request):
        company_filter = filters.CompanyFilter(request.GET)
        company_table = tables.CompanyTable(company_filter.qs)
        RequestConfig(
            request, paginate={
                'per_page': 50
            }).configure(company_table)
        context = {'table': company_table, 'filter': company_filter}

        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            exporter = TableExport(export_format, company_table)
            return exporter.response('table.{}'.format(export_format))

        return render(request, 'company/display_company.html', context)

    def add(self, request):
        form = CompanyForm()
        dates_form = formset_factory(
            DatesManagerForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseDatesFormSet
        )(prefix='dates')
        key_person_form = formset_factory(
            CompanyKeyPersonForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseDatesFormSet
        )(prefix='key_person')

        return render(request, 'company/add_company.html', {
            'form': form,
            'date_form': dates_form,
            'key_person_form': key_person_form
        })

    def post(self, request):
        form = CompanyForm(request.POST, request.FILES)
        dates_form = formset_factory(
            DatesManagerForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseDatesFormSet,
        )(request.POST, prefix='dates')
        key_person_form = formset_factory(
            CompanyKeyPersonForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseDatesFormSet
        )(request.POST, prefix='key_person')
        form_error = False

        if form.is_valid() and dates_form.is_valid():
            for date in dates_form:
                if not date.is_valid():
                    form_error = True

            for key in key_person_form:
                if not key.is_valid():
                    form_error = True

            if not form_error:
                company = form.save()

                for date in dates_form:
                    new_date = date.save()
                    company.appointing_cease_date.add(new_date)
                
                for key in key_person_form:
                    new_key = key.save()
                    company.key_person.add(new_key)
                return redirect('/master/')
        return render(request, 'company/add_company.html', {
            'form': form,
            'date_form': dates_form,
            'key_person_form': key_person_form
        })


class DistributorView(View):

    def get(self, request):
        return self.add(request) if 'add' in request.GET\
            else self.display(request)

    def add(self, request):
        form = DistributorForm()
        dates_form = formset_factory(
            DatesManagerForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseDatesFormSet
        )(prefix='dates')
        key_person_form = formset_factory(
            CompanyKeyPersonForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseDatesFormSet
        )(prefix='key_person')
        return render(request, 'distributor/add_distributor.html', {
            'form': form,
            'date_form': dates_form,
            'key_person_form': key_person_form
        })

    def post(self, request):
        form = DistributorForm(request.POST, request.FILES)
        dates_form = formset_factory(
            DatesManagerForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseDatesFormSet,
        )(request.POST, prefix='dates')
        key_person_form = formset_factory(
            CompanyKeyPersonForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=BaseDatesFormSet
        )(request.POST, prefix='key_person')
        form_error = False

        if form.is_valid() and dates_form.is_valid():
            for date in dates_form:
                if not date.is_valid():
                    form_error = True

            for key in key_person_form:
                if not key.is_valid():
                    form_error = True

            if not form_error:
                distributor = form.save()

                for date in dates_form:
                    new_date = date.save()
                    distributor.appointing_cease_date.add(new_date)

                for key in key_person_form:
                    new_key = key.save()
                    distributor.key_person.add(new_key)
                return redirect('/master/')
        return render(request, 'distributor/add_company.html', {
            'form': form,
            'date_form': dates_form,
            'key_person_form': key_person_form
        })

    def display(self, request):
        distributor_filter = filters.DistributorFilter(request.GET)
        distributor_table = tables.DistributorTable(distributor_filter.qs)
        RequestConfig(
            request, paginate={
                'per_page': 50
            }).configure(distributor_table)
        context = {'table': distributor_table, 'filter': distributor_filter}

        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            exporter = TableExport(export_format, distributor_table)
            return exporter.response('table.{}'.format(export_format))

        return render(request, 'distributor/display_distributor.html', context)


class UnitView(View):
    def get(self, request):
        return self.add(request) if 'add' in request.GET \
            else self.display(request)

    def display(self, request):
        unit_filter = filters.UnitFilter(request.GET)
        unit_table = tables.UnitTable(unit_filter.qs)
        RequestConfig(request, paginate={'per_page': 50}).configure(unit_table)
        context = {'table': unit_table, 'filter': unit_filter}

        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            exporter = TableExport(export_format, unit_table)
            return exporter.response('table.{}'.format(export_format))

        return render(request, 'unit/display_unit.html', context)

    def add(self, request):
        form = UnitForm()
        return render(request, 'unit/add_unit.html', {'form': form})

    def post(self, request):
        form = UnitForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS,
                                 'Unit ' + form.data['name'] + ' Added')
            return redirect("/master")
        return render(request, 'unit/add_unit.html', {'form': form})


class CompanyKeyPersonView(View):
    def get(self, request):
        return self.add(request) if 'add' in request.GET \
            else self.display(request)

    def add(self, request):
        form = CompanyKeyPersonForm()
        return render(request, 'companykeyperson/add_key_person.html', {'form': form})

    def post(self, request):
        form = CompanyKeyPersonForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/master")
        return render(request, 'companykeyperson/add_key_person.html', {'form': form})


def master_home(request):
    model_cards = []
    not_required_model = ['DatesManager', 'CompanyKeyPerson', 'BankDetail', 'Unit']
    for model in [
            each_model for each_model in required_model
            if each_model not in not_required_model
    ]:
        table = getattr(models, model)
        card = {}
        qs = table.objects.all()
        card['name'] = model
        card['count'] = qs.count()
        card['last_entry'] = qs.latest('created_at').created_at \
            if qs.exists() else ''
        model_cards.append(card)
        card['link'] = model.lower()
    return render(request, 'master_home.html', {"cards": model_cards})
