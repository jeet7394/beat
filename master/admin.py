from django.contrib import admin

# Register your models here.
from master import models

required_model = [
    'Company',
    'CompanyKeyPerson',
    'DatesManager',
    'Designation',
    'Employee',
    'Product',
    'ProductGroup',
    'State',
    'Town',
    'Unit',
    'District',
    'Distributor'
]

for model in required_model:
    admin.site.register(getattr(models, model))
