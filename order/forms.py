from django import forms
from djangoformsetjs.utils import formset_media_js
from django.forms import BaseFormSet
from django.utils.timezone import now
from order import models
from master import models as master_models
from master.forms import RelatedFieldWidgetCanAdd, \
    MultipleRelatedFieldWidgetCanAdd


class OrderForm(forms.ModelForm):

    class Meta:
        model = models.Order
        fields = ('__all__')


class ProductFormSet(BaseFormSet):
    def clean(self):
        if any(self.errors):
            return


class ProductRowForm(forms.ModelForm):

    class Meta:
        model = models.OrderProductRow
        fields = ('__all__')
        widgets = {
            'product':
            RelatedFieldWidgetCanAdd(
                related_model=master_models.Product,
                url='/order/auto-complete/product/',
                related_url='/master/product/?add',
                forward=['product_group']
            ),
        }

    class Media(object):
        js = formset_media_js


class OrderFormPart1(forms.ModelForm):

    date = forms.DateField()

    class Meta:
        model = models.Order
        fields = (
            'product_group', 'company', 'date', 'order_number', 'distributor',
            'town', 'bar_rate', 'bill_rate', 's_p_d', 'm_r_p',
        )
        widgets = {
            'product_group':
            RelatedFieldWidgetCanAdd(
                related_model=master_models.ProductGroup,
                url='/order/auto-complete/product-group/',
                related_url='/master/productgroup/?add'
            ),
            'company':
            RelatedFieldWidgetCanAdd(
                related_model=master_models.Company,
                url='/order/auto-complete/company/',
                related_url='/master/company/?add',
                forward=['product_group']
            ),
            'distributor':
            RelatedFieldWidgetCanAdd(
                related_model=master_models.Distributor,
                url='/order/auto-complete/distributor/',
                related_url='/master/distributor/?add',
                forward=['product_group', 'company']
            ),
            'town':
            RelatedFieldWidgetCanAdd(
                related_model=master_models.Town,
                url='/order/auto-complete/town/',
                related_url='/master/town/?add',
            ),
        }


class OrderFormPart2(forms.ModelForm):

    class Meta:
        model = models.Order
        fields = (
            'total_quantity', 'total_cases', 'total_weight',
            'special_instruction', 'dispatch_firm_name', 'dispatch_address',
            'dispatch_gst_number', 'status'
        )

