from django.urls import path
from order import auto_complete

urlpatterns = [
    path(
        'state/',
        auto_complete.StateAutoComplete.as_view(),
        name='state-autocomplete',
    ),
    path(
        'town/',
        auto_complete.TownAutoComplete.as_view(),
        name='town-autocomplete'
    ),
    path(
        'district/',
        auto_complete.DistrictAutoComplete.as_view(),
        name='district-autocomplete'
    ),
    path(
        'unit/',
        auto_complete.UnitAutoComplete.as_view()
    ),
    path(
        'product-group/',
        auto_complete.ProductGroupAutoComplete.as_view()
    ),
    path(
        'product/',
        auto_complete.ProductAutoComplete.as_view()
    ),
    path(
        'designation/',
        auto_complete.DesignationAutoComplete.as_view()
    ),
    path(
        'employee/',
        auto_complete.EmployeeDetailAutoComplete.as_view()
    ),
    path(
        'company-key-person/',
        auto_complete.CompanyKeyPersonAutoComplete.as_view()
    ),
    path(
        'company/',
        auto_complete.CompanyAutoComplete.as_view()
    ),
    path(
        'distributor/',
        auto_complete.DistributorAutoComplete.as_view()
    ),
    path(
        'special_instruction/',
        auto_complete.SpecialInstruction.as_view(create_field='content')
    )
]
