from django.urls import path, include
from order import views

urlpatterns = [
    path('', views.OrderView.as_view()),
    path(
        "auto-complete/",
        include("order.auto_complete_urls")
    ),
    path(
        "order_number/",
        views.generate_order_number
    ),
    path(
        "edit/<int:pk>/",
        views.OrderEditView.as_view(),
        name="edit_order"
    ),
    path(
        "update_satus/<int:pk>/<int:type>",
        views.update_order_status,
        name="update_order_status"
    ),
    path(
        "generate_pdf/<int:pk>",
        views.generate_pdf,
        name="order_pdf"
    ),
    path(
        "product_detail/<int:pk>",
        views.get_product_details
    )
]
