from django.db import models
from django.core.validators import RegexValidator
from django.conf import settings
from django.utils.safestring import mark_safe
from master import models as master_models


class SpecialInstruction(models.Model):
    content = models.TextField()

    def __str__(self):
        return self.content


class OrderProductRow(models.Model):
    product = models.ForeignKey(master_models.Product, models.PROTECT)
    product_quantity = models.IntegerField()
    product_cases = models.IntegerField()
    product_weight = models.FloatField()
    product_mrp = models.FloatField()

    def __str__(self):
        return self.product.name


class Order(models.Model):
    product_group = models.ForeignKey(
        master_models.ProductGroup,
        models.PROTECT
    )
    company = models.ForeignKey(master_models.Company, models.PROTECT)
    order_date = models.DateField(auto_now=True)
    order_number = models.CharField(max_length=255)
    distributor = models.ForeignKey(master_models.Distributor, models.PROTECT)
    town = models.ForeignKey(master_models.Town, models.PROTECT)
    bar_rate = models.FloatField()
    bill_rate = models.FloatField()
    s_p_d = models.FloatField()
    m_r_p = models.FloatField()
    total_quantity = models.IntegerField()
    total_cases = models.IntegerField()
    total_weight = models.FloatField()
    special_instruction = models.TextField()

    products = models.ManyToManyField(
        OrderProductRow,
        blank=True
    )

    # Dispatch To
    dispatch_firm_name = models.CharField(
        max_length=255,
        verbose_name="Firm Name"
    )
    dispatch_address = models.TextField()
    dispatch_gst_number = models.CharField(
        max_length=255,
        validators=[
            RegexValidator(
                regex=r'^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$'
            )
        ]
    )

    status = models.CharField(
        max_length=255,
        choices=(
            ("Pending", "Pending"),
            ("Processing", "Processing"),
            ("Delivered", "Delivered")
        ),
        default="Pending"
    )

    added_by = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)

    def __str__(self):
        return "Order of ("\
            + self.distributor.firm_name + ") "\
            + self.town.name

    @property
    def delete_order(self):
        if self.status == "Pending":
            return mark_safe("""<a href='/order/delete/{0}'>
                <span class="delete-icon" uk-icon="trash"></span>
            </a>""".format(self.id))
        return ""

    @property
    def edit_order(self):
        if self.status == "Pending":
            return mark_safe("""<a href='/order/edit/{0}'>
                <span uk-icon="pencil"></span>
            </a>""".format(self.id))
        return ""

    @property
    def update_status_handler(self):
        string = ""
        if self.status != "Delivered":
            string += """
                <a href='/order/update_satus/{0}/0'>
                    <span uk-icon="push"></span>
                </a>
            """
        if self.status != "Pending":
            string += """
                <a href='/order/update_satus/{0}/1'>
                    <span uk-icon="pull"></span>
                </a>
            """
        return mark_safe(string.format(self.id))
