from django.shortcuts import render, HttpResponse, redirect, Http404
from django.http import JsonResponse
from django.forms import formset_factory
from django.views import View
from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
import xhtml2pdf.pisa as pisa
from django_tables2 import RequestConfig
from django_tables2.export.export import TableExport
from order import forms, models, tables, filters
from order.utils import generate_product_dict
from master import models as master_models


class OrderView(View):

    def get(self, request):
        return self.add(request) if 'add' in request.GET \
            else self.display(request)

    def add(self, request):
        part_1_form = forms.OrderFormPart1()
        part_2_form = forms.OrderFormPart2()
        product_row_form = formset_factory(
            forms.ProductRowForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=forms.ProductFormSet
        )(prefix='product_row')
        context = {
            'form': part_1_form,
            'form2': part_2_form,
            'product_row_form': product_row_form
        }
        return render(request, "order/add_order.html", context)

    def display(self, request):
        order_filter = filters.OrderFilter(request.GET)
        order_table = tables.OrderTable(order_filter.qs)
        RequestConfig(
            request, paginate={
                'per_page': 50
            }).configure(order_table)
        context = {'table': order_table, 'filter': order_filter}

        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            exporter = TableExport(export_format, order_table)
            return exporter.response('table.{}'.format(export_format))

        return render(request, "order/display_order.html", context)

    def post(self, request):
        post_data = request.POST.copy()
        post_data['added_by'] = request.user.id
        form = forms.OrderForm(post_data)
        part_1_form = forms.OrderFormPart1(request.POST)
        part_2_form = forms.OrderFormPart2(request.POST)
        product_row_form = formset_factory(
            forms.ProductRowForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=forms.ProductFormSet
        )(request.POST, prefix='product_row')

        form_error = False
        if form.is_valid() and product_row_form.is_valid():
            for product in product_row_form:
                if not product.is_valid():
                    form_error = True

            if not form_error:
                order = form.save()

                for product in product_row_form:
                    new_product = product.save()
                    order.products.add(new_product)
                return redirect('/master/')
        context = {
            'form': part_1_form,
            'form2': part_2_form,
            'product_row_form': product_row_form
        }
        return render(request, "order/add_order.html", context)


class OrderEditView(View):

    def getObject(self, pk):
        try:
            return models.Order.objects.get(id=pk)
        except:
            raise Http404

    def get(self, request, pk):
        obj = self.getObject(pk)
        part_1_form = forms.OrderFormPart1(instance=obj)
        part_2_form = forms.OrderFormPart2(instance=obj)
        product_row_form = formset_factory(
            forms.ProductRowForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=forms.ProductFormSet
        )(generate_product_dict(obj, 'products'), prefix='product_row')

        for each_err in product_row_form.errors:
            if 'product' in each_err:
                each_err['product'] = ""
        context = {
            'form': part_1_form,
            'form2': part_2_form,
            'product_row_form': product_row_form
        }
        return render(request, "order/add_order.html", context)

    def post(self, request, pk):
        obj = self.getObject(pk)
        post_data = request.POST.copy()
        post_data['added_by'] = request.user.id

        form = forms.OrderForm(post_data, instance=obj)
        part_1_form = forms.OrderFormPart1(request.POST)
        part_2_form = forms.OrderFormPart2(request.POST)
        product_row_form = formset_factory(
            forms.ProductRowForm,
            extra=1,
            can_delete=True,
            validate_min=True,
            formset=forms.ProductFormSet
        )(request.POST, prefix='product_row')

        form_error = False
        if form.is_valid() and product_row_form.is_valid():
            for product in product_row_form:
                if not product.is_valid():
                    form_error = True

            if not form_error:
                order = form.save()
                order.products.clear()

                for product in product_row_form:
                    new_product = product.save()
                    order.products.add(new_product)
                return redirect('/order/')
        context = {
            'form': part_1_form,
            'form2': part_2_form,
            'product_row_form': product_row_form
        }
        return render(request, "order/add_order.html", context)


def generate_pdf(request, pk):
    order = models.Order.objects.get(id=pk)
    return render_to_pdf("order/pdf.html", {'order': order})


def generate_order_number(request):
    pg_code = master_models.ProductGroup.objects.get(id=request.GET['p'])
    c_code = master_models.Company.objects.get(id=request.GET['c'])
    order = models.Order.objects.order_by('-pk').count()
    return HttpResponse("{0}/{1}/{2}".format(
        pg_code.code,
        c_code.code,
        order + 1
    ))


def update_order_status(request, pk, type):
    status = {
        "Pending": "Processing",
        "Processing": "Delivered",
    } if type == 0 else {
        "Processing": "Pending",
        "Delivered": "Processing",
    }

    order = models.Order.objects.get(id=pk)
    order.status = status[order.status]
    order.save()
    return redirect("/order/")


def get_product_details(request, pk):
    product = master_models.Product.objects.get(id=pk)
    pro_obj = {
        "name": product.name,
        "casses": product.current_case_size,
        "content": product.content,
        "weight": product.unit.weight_of_single_unit,
        "decimal": product.unit.decimal_places
    }
    if product.base_for_mrp:
        pro_obj['mrp'] = product.default_mrp
    if product.base_for_rate:
        pro_obj['rate'] = product.default_rate
    return JsonResponse(pro_obj, safe=False)


def render_to_pdf(path: str, params: dict):
    template = get_template(path)
    html = template.render(params)
    response = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
    if not pdf.err:
        return HttpResponse(response.getvalue(), content_type='application/pdf')
    return HttpResponse("Error Rendering PDF", status=400)
