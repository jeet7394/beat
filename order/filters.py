from django_filters import FilterSet, CharFilter
from django.db.models import CharField
from django.db.models import Q
from order import models


class OrderFilter(FilterSet):

    name = CharFilter(name='name', method='filter_name')

    class Meta:
        model = models.Order
        fields = {'name'}

    def filter_name(self, queryset, name, value):
        fields = [f for f in self.Meta.model._meta.get_fields() if isinstance(
            f, CharField
        )]

        queries = [Q(**{f.name + '__icontains': value}) for f in fields]
        qs = Q()
        for query in queries:
            qs = qs | query
        return self.Meta.model.objects.filter(qs)