from dal import autocomplete
from master.models import State, District, ProductGroup, Unit, Designation, \
    Town, Employee, Product, CompanyKeyPerson, \
    Company, Distributor
from order import models


class StateAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = State.objects.all().order_by('name')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class DistrictAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = District.objects.all().order_by('name')
        state = self.forwarded.get(
            'state',
            self.forwarded.get(
                'headquarter_state',
                None
            )
        )
        if state:
            qs = qs.filter(state_id=int(state))

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class TownAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Town.objects.all().order_by('name')

        district = self.forwarded.get(
            'district',
            self.forwarded.get(
                'headquarter_district',
                None
            )
        )
        if district:
            qs = qs.filter(district_id=int(district))
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class ProductAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Product.objects.all().order_by('name')
        qs = qs.filter(
            group=self.forwarded['product_group'],
            launch_discontinuation_date__end_date__isnull=True
        )

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class ProductGroupAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = ProductGroup.objects.all().order_by('name')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class UnitAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Unit.objects.all().order_by('name')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class DesignationAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Designation.objects.all().order_by('name')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class EmployeeDetailAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Employee.objects.all().order_by('name')
        designation = self.forwarded.get('designation', None)

        if designation:
            qs = qs.filter(designation=Designation.objects.get(
                id=int(designation)).reporting_designation
            )

        if self.forwarded.get('field_staff', None):
            qs = qs.filter(designation__work_area='field')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class CompanyKeyPersonAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = CompanyKeyPerson.objects.all().order_by('name')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs


class CompanyAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Company.objects.all().order_by('firm_name')

        if self.forwarded.get('product_group'):
            qs = qs.filter(product_group=self.forwarded.get('product_group'))

        if self.q:
            qs = qs.filter(firm_name__istartswith=self.q)
        return qs


class DistributorAutoComplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Distributor.objects.all().order_by('firm_name')
        qs = qs.filter(appointing_cease_date__end_date__isnull=True)

        if self.forwarded.get("product_group"):
            qs = qs.filter(product_group=self.forwarded.get("product_group"))

        if self.q:
            qs = qs.filter(firm_name__istartswith=self.q)
        return qs


class SpecialInstruction(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = models.SpecialInstruction.objects.all()

        if self.q:
            qs = qs.filter(content__istartswith=self.q)
        return qs
