import django_tables2 as tables
from django_tables2.utils import A
from django.utils.safestring import mark_safe
from order import models


class OrderTable(tables.Table):

    updae_status_field = tables.Column(
        accessor='update_status_handler',
        verbose_name='Update Status',
        orderable=False
    )

    delete_field = tables.Column(
        accessor='delete_order',
        verbose_name='Delete',
        orderable=False
    )

    edit_field = tables.Column(
        accessor='edit_order',
        verbose_name='Edit',
        orderable=False
    )

    pdf = tables.LinkColumn(
        'order_pdf',
        args=[A('pk')],
        text=mark_safe('<span uk-icon="download"></span>'),
        orderable=False
    )

    class Meta:
        model = models.Order
        attrs = {'class': 'uk-table uk-table-striped uk-table-divider'}
        exclude = ('special_instruction',)
